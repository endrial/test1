  $('.ui.form')
    .form({
      fields: {
        name: {
          identifier: 'name',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter your full name'
            }
          ]
        },
        email: {
          identifier  : 'email',
          rules: [
            {
              type   : 'email',
              prompt : 'Please enter a valid e-mail'
            }
          ]
        },
        username: {
          identifier: 'username',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter a username'
            }
          ]
        },
        password: {
          identifier: 'password',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter a password'
            },
            {
              type   : 'minLength[6]',
              prompt : 'Your password must be at least {ruleValue} characters'
            }
          ]
        },
        confirmPassword: {
          identifier  : 'confirmPassword',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter a confirm password'
            },
            {
              type   : 'match[password]',
              prompt : 'Your passwords do not match.'
            }
          ]
        },
        maxLength: {
          identifier  : 'maxLength',
          rules: [
            {
              type   : 'maxLength[100]',
              prompt : 'Please enter at most 100 characters'
            }
          ]
        },
        number: {
          identifier  : 'number',
          rules: [
            {
              type   : 'number',
              prompt : 'Please enter a valid number'
            }
          ]
        },
        terms: {
          identifier: 'terms',
          rules: [
            {
              type   : 'checked',
              prompt : 'You must agree to the terms and conditions'
            }
          ]
        }
      }
    })
  ;